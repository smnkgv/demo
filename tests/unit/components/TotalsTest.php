<?php

namespace tests\components;

use components\Totals;

class TotalsTest extends \Codeception\TestCase\Test
{
    private $orders;
    private $orderDetails;
    private $suppliersWithOc;

    public function _before()
    {
        $this->orders = [
            [
                'Ordernumber'          => '91266247x',
                'order_id'             => '91266247',
                'RegId'                => '185566',
                'Status'               => '1',
                'OrderDate'            => '2016-07-08 12:10:49',
                'Reference'            => '',
                'DelCompany'           => '',
                'DelDepartment'        => '',
                'DelName'              => 'Alexius Mih',
                'delprefix'            => '',
                'delstreet'            => 'Nieuwe Steen',
                'DelNbr'               => '18',
                'DelCity'              => 'HOORN NH',
                'DelZip'               => '1625HV',
                'DelCountry'           => 'Nederland',
                'DelPhone'             => '020-3472323',
                'DelFax'               => '0632344567',
                'DelEmail'             => 'alex@bintime.com',
                'Currency'             => 'EUR',
                'Shipping'             => '',
                'ShippingCosts'        => '0.00',
                'Discount'             => '64.00',
                'tax'                  => '0.00',
                'total'                => '0.00',
                'totaltopay'           => '0.00',
                'PaymentMethod'        => 'prepay',
                'Paysubid'             => '',
                'DeliveryMethod'       => 'CUSTOMER',
                'Updated'              => '2016-07-08 12:10:49',
                'Remarks'              => '
Gebruikte (korting)codes:
52fvd16j648wdar2',
                'ProposalBy'           => '',
                'visible'              => 'true',
                'delextension'         => '',
                'dellastname'          => '',
                'paymenttry'           => '1',
                'DelSex'               => 'heer',
                'shippingcosts_distri' => '5.70',
                'unit_type'            => '4',
                'eigenaar_id'          => '1',
                'onsite_id'            => '3',
                'gateway_id'           => '1',
            ]
        ];

        $this->orderDetails = [
            [
                'Ordernumber'            => '91266247x',
                'order_id'               => '91266247',
                'Volgnr'                 => '1',
                'ProdLevId'              => '18630',
                'name'                   => 'T573 PicturePack (C13T57304010)',
                'CatId'                  => '14121812',
                'Vendor'                 => 'Epson',
                'EUprice'                => '64.00',
                'Quant'                  => '1',
                'Pprice'                 => '37.69',
                'Updated'                => '2016-07-08 12:10:49',
                'Supplier'               => 'API',
                'ProdId'                 => 'C13T57304010',
                'visible'                => 'true',
                'tax_rate'               => '0.21',
                'discount'               => '64.00',
                'composite_product_hash' => '',
                'expected_delivery'      => null,
                'status'                 => '1',
                'reference'              => null,
            ]
        ];

        $this->suppliersWithOc = [
            [
                'supplier_id' => '89',
                'name' => 'API',
                'code' => 'API',
                'last_import' => NULL,
                'credit' => NULL,
                'partial_del' => NULL,
                'hpflex' => NULL,
                'contact_mail' => '',
                'rebate_perc' => '0.00',
                'is_broker' => 'true',
                'official_distri' => 'false',
                'cutoff_time' => '16:00',
                'oc' => '5.70',
                'sc' => '0.00',
                'treshhold' => '0.00',
                'oc_sdc_below_threshold' => '6.50',
                'oc_sdc_above_threshold' => NULL,
                'round_val' => 'true',
            ]
        ];
    }

    public function testCostPlus()
    {
        $totals = new Totals();
        $result = $totals->calcCostPlus($this->orderDetails, $this->suppliersWithOc, $this->orders);

        $this->assertEquals(-43.39, $result);
    }

    public function testCostPlusInPercents()
    {
        $totals = new Totals();
        $result = $totals->calcCostPlusInPercents($this->orderDetails, $this->suppliersWithOc, $this->orders);

        $this->assertEquals(-100.00, $result);
    }

    public function testTotalsSumBySuppliers()
    {
        $totals = new Totals();
        $result = $totals->calcTotalsSumBySuppliers($this->orderDetails, $this->suppliersWithOc);

        $this->assertEquals(43.39, $result);
    }

    public function testTotalsBySuppliers()
    {
        $totals = new Totals();
        $result = $totals->calcTotalsBySuppliers($this->orderDetails, $this->suppliersWithOc);

        $this->assertEquals(['API' => 43.39], $result);

    }

    public function testPurchasingBySuppliers()
    {
        $totals = new Totals();
        $result = $totals->calcPurchasingBySuppliers($this->orderDetails);

        $this->assertEquals(['API' =>  37.69], $result);
    }

    public function testCalcSubtotal()
    {
        $totals = new Totals();
        $result = $totals->calcSubtotal($this->orderDetails, $this->orders);

        $this->assertEquals(0, $result);
    }

    public function testSumOfEndUserTotals()
    {
        $totals = new Totals();
        $result = $totals->calcSumOfEndUserTotals($this->orderDetails);

        $this->assertEquals(64, $result);
    }

    public function testTotalDiscount()
    {
        $totals = new Totals();
        $result = $totals->calcTotalDiscount($this->orderDetails);

        $this->assertEquals(64, $result);
    }

    public function testShipping()
    {
        $totals = new Totals();
        $result = $totals->calcShipping($this->orders);

        $this->assertEquals(0, $result);
    }
}
