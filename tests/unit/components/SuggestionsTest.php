<?php

namespace tests\components;

use components\suggestions\CombinationsGenerator;
use components\suggestions\criteria\BrokerCriteria;
use components\suggestions\criteria\SuppliersCostCriteria;
use components\suggestions\criteria\SuppliersCountCriteria;
use components\suggestions\Suggestions;

class SuggestionsTest extends \Codeception\TestCase\Test
{
    private $dbMock;

    private $criteriaMock;

    private $suggestions;

    private $productsPool;

    private function mockDb()
    {
        $mockCmd = $this->getMockBuilder('yii\db\Command')->setMethods(['queryAll'])->getMock();
        $mockCmd->method('queryAll')->willReturn($this->productsPool);

        $mock = $this->getMockBuilder('yii\db\Connection')->setMethods(['createCommand'])->getMock();
        $mock->method('createCommand')->willReturn($mockCmd);

        $this->dbMock = $mock;
    }

    private function mockSuggestions()
    {
        $this->suggestions = [
            [
                'products' =>
                    [
                        0 => 0,
                        1 => 4,
                        2 => 9,
                    ],
            ],
            [
                'products' =>
                    [
                        0 => 3,
                        1 => 4,
                        2 => 9,
                    ],
            ],
            [
                'products' =>
                    [
                        0 => 1,
                        1 => 2,
                        2 => 3,
                    ],
            ],
            [
                'products' =>
                    [
                        0 => 4,
                        1 => 1,
                        2 => 7,
                    ],
            ],
        ];
    }

    private function mockProducts()
    {
        $this->productsPool = [
            [
                'order_id'           => '91266258',
                'Ordernumber'        => '91266258u',
                'Volgnr'             => '1',
                'Vendor'             => 'HP',
                'ProdId'             => 'J9A91EA#ABB',
                'original_ProdLevId' => '3740243',
                'original_supp'      => 'TD',
                'Quant'              => '1',
                'discount'           => null,
                'EUprice'            => '347.00',
                'ShippingCosts'      => '0.00',
                'supplier'           => 'AC',
                'ProdLevId'          => '2528276',
                'Visible'            => 'N',
                'Stock'              => '0',
                'pprice'             => '325.730',
                'original_pprice'    => '322.15',
                'EOL'                => '0',
                'HPE'                => '0',
                'name'               => 'Actebis',
                'is_broker'          => 'false',
                'rebate_perc'        => '0.00',
                'official_distri'    => 'false',
                'oc'                 => '4.50',
                'sc'                 => '0.00',
                'treshhold'          => '10.00',
            ],
            [
                'order_id'           => '91266258',
                'Ordernumber'        => '91266258u',
                'Volgnr'             => '1',
                'Vendor'             => 'HP',
                'ProdId'             => 'J9A91EA#ABB',
                'original_ProdLevId' => '3740243',
                'original_supp'      => 'TD',
                'Quant'              => '1',
                'discount'           => null,
                'EUprice'            => '347.00',
                'ShippingCosts'      => '0.00',
                'supplier'           => 'CO',
                'ProdLevId'          => 'CO-HPJ9A91EA-ABB',
                'Visible'            => 'N',
                'Stock'              => '1',
                'pprice'             => '321.180',
                'original_pprice'    => '322.15',
                'EOL'                => '0',
                'HPE'                => '0',
                'name'               => 'Copaco',
                'is_broker'          => 'false',
                'rebate_perc'        => '0.70',
                'official_distri'    => 'true',
                'oc'                 => '8.40',
                'sc'                 => '0.00',
                'treshhold'          => '0.00',
            ],
            [
                'order_id'           => '91266258',
                'Ordernumber'        => '91266258u',
                'Volgnr'             => '1',
                'Vendor'             => 'HP',
                'ProdId'             => 'J9A91EA#ABB',
                'original_ProdLevId' => '3740243',
                'original_supp'      => 'TD',
                'Quant'              => '1',
                'discount'           => null,
                'EUprice'            => '347.00',
                'ShippingCosts'      => '0.00',
                'supplier'           => 'IM',
                'ProdLevId'          => '3142857',
                'Visible'            => 'N',
                'Stock'              => '0',
                'pprice'             => '321.990',
                'original_pprice'    => '322.15',
                'EOL'                => '0',
                'HPE'                => '0',
                'name'               => 'Ingram Micro',
                'is_broker'          => 'false',
                'rebate_perc'        => '0.70',
                'official_distri'    => 'true',
                'oc'                 => '8.00',
                'sc'                 => '0.00',
                'treshhold'          => '0.00',
            ],
            [
                'order_id'           => '91266258',
                'Ordernumber'        => '91266258u',
                'Volgnr'             => '1',
                'Vendor'             => 'HP',
                'ProdId'             => 'J9A91EA#ABB',
                'original_ProdLevId' => '3740243',
                'original_supp'      => 'TD',
                'Quant'              => '1',
                'discount'           => null,
                'EUprice'            => '347.00',
                'ShippingCosts'      => '0.00',
                'supplier'           => 'TV',
                'ProdLevId'          => 'HPJ9A91EA',
                'Visible'            => 'N',
                'Stock'              => '0',
                'pprice'             => '321.600',
                'original_pprice'    => '322.15',
                'EOL'                => '0',
                'HPE'                => '0',
                'name'               => 'Travion',
                'is_broker'          => 'true',
                'rebate_perc'        => '0.37',
                'official_distri'    => 'false',
                'oc'                 => '7.75',
                'sc'                 => '0.00',
                'treshhold'          => '0.00',
            ],
            [
                'order_id'           => '91266258',
                'Ordernumber'        => '91266258u',
                'Volgnr'             => '1',
                'Vendor'             => 'HP',
                'ProdId'             => 'J9A91EA#ABB',
                'original_ProdLevId' => '3740243',
                'original_supp'      => 'TD',
                'Quant'              => '1',
                'discount'           => null,
                'EUprice'            => '347.00',
                'ShippingCosts'      => '0.00',
                'supplier'           => 'TD',
                'ProdLevId'          => '3740243',
                'Visible'            => 'Y',
                'Stock'              => '25',
                'pprice'             => '322.150',
                'original_pprice'    => '322.15',
                'EOL'                => '0',
                'HPE'                => '0',
                'name'               => 'TechData',
                'is_broker'          => 'false',
                'rebate_perc'        => '0.40',
                'official_distri'    => 'true',
                'oc'                 => '7.95',
                'sc'                 => '0.00',
                'treshhold'          => '0.00',
            ],
            [
                'order_id'           => '91266258',
                'Ordernumber'        => '91266258c',
                'Volgnr'             => '2',
                'Vendor'             => 'HP',
                'ProdId'             => 'B3G86A#B19',
                'original_ProdLevId' => 'CO-HPB3G86A-B19',
                'original_supp'      => 'CO',
                'Quant'              => '1',
                'discount'           => null,
                'EUprice'            => '3656.00',
                'ShippingCosts'      => '0.00',
                'supplier'           => 'AC',
                'ProdLevId'          => '2165368',
                'Visible'            => 'N',
                'Stock'              => '1',
                'pprice'             => '3706.370',
                'original_pprice'    => '3476.13',
                'EOL'                => '0',
                'HPE'                => '0',
                'name'               => 'Actebis',
                'is_broker'          => 'false',
                'rebate_perc'        => '0.00',
                'official_distri'    => 'false',
                'oc'                 => '4.50',
                'sc'                 => '0.00',
                'treshhold'          => '10.00',
            ],
            [
                'order_id'           => '91266258',
                'Ordernumber'        => '91266258c',
                'Volgnr'             => '2',
                'Vendor'             => 'HP',
                'ProdId'             => 'B3G86A#B19',
                'original_ProdLevId' => 'CO-HPB3G86A-B19',
                'original_supp'      => 'CO',
                'Quant'              => '1',
                'discount'           => null,
                'EUprice'            => '3656.00',
                'ShippingCosts'      => '0.00',
                'supplier'           => 'EET',
                'ProdLevId'          => 'EET-B3G86A#B19',
                'Visible'            => 'N',
                'Stock'              => '0',
                'pprice'             => '3526.000',
                'original_pprice'    => '3476.13',
                'EOL'                => '0',
                'HPE'                => '0',
                'name'               => 'EET Nordic DK',
                'is_broker'          => 'false',
                'rebate_perc'        => '0.50',
                'official_distri'    => 'false',
                'oc'                 => '8.50',
                'sc'                 => '5.00',
                'treshhold'          => '100.00',
            ],
            [
                'order_id'           => '91266258',
                'Ordernumber'        => '91266258c',
                'Volgnr'             => '2',
                'Vendor'             => 'HP',
                'ProdId'             => 'B3G86A#B19',
                'original_ProdLevId' => 'CO-HPB3G86A-B19',
                'original_supp'      => 'CO',
                'Quant'              => '1',
                'discount'           => null,
                'EUprice'            => '3656.00',
                'ShippingCosts'      => '0.00',
                'supplier'           => 'IM',
                'ProdLevId'          => '2724128',
                'Visible'            => 'N',
                'Stock'              => '0',
                'pprice'             => '3511.110',
                'original_pprice'    => '3476.13',
                'EOL'                => '0',
                'HPE'                => '0',
                'name'               => 'Ingram Micro',
                'is_broker'          => 'false',
                'rebate_perc'        => '0.70',
                'official_distri'    => 'true',
                'oc'                 => '8.00',
                'sc'                 => '0.00',
                'treshhold'          => '0.00',
            ],
            [
                'order_id'           => '91266258',
                'Ordernumber'        => '91266258c',
                'Volgnr'             => '2',
                'Vendor'             => 'HP',
                'ProdId'             => 'B3G86A#B19',
                'original_ProdLevId' => 'CO-HPB3G86A-B19',
                'original_supp'      => 'CO',
                'Quant'              => '1',
                'discount'           => null,
                'EUprice'            => '3656.00',
                'ShippingCosts'      => '0.00',
                'supplier'           => 'TD',
                'ProdLevId'          => '3193721',
                'Visible'            => 'N',
                'Stock'              => '0',
                'pprice'             => '3476.000',
                'original_pprice'    => '3476.13',
                'EOL'                => '0',
                'HPE'                => '0',
                'name'               => 'TechData',
                'is_broker'          => 'false',
                'rebate_perc'        => '0.40',
                'official_distri'    => 'true',
                'oc'                 => '7.95',
                'sc'                 => '0.00',
                'treshhold'          => '0.00',
            ],
            [
                'order_id'           => '91266258',
                'Ordernumber'        => '91266258c',
                'Volgnr'             => '2',
                'Vendor'             => 'HP',
                'ProdId'             => 'B3G86A#B19',
                'original_ProdLevId' => 'CO-HPB3G86A-B19',
                'original_supp'      => 'CO',
                'Quant'              => '1',
                'discount'           => null,
                'EUprice'            => '3656.00',
                'ShippingCosts'      => '0.00',
                'supplier'           => 'CO',
                'ProdLevId'          => 'CO-HPB3G86A-B19',
                'Visible'            => 'Y',
                'Stock'              => '1',
                'pprice'             => '3476.130',
                'original_pprice'    => '3476.13',
                'EOL'                => '0',
                'HPE'                => '0',
                'name'               => 'Copaco',
                'is_broker'          => 'false',
                'rebate_perc'        => '0.70',
                'official_distri'    => 'true',
                'oc'                 => '8.40',
                'sc'                 => '0.00',
                'treshhold'          => '0.00',
            ],
            [
                'order_id'           => '91266258',
                'Ordernumber'        => '91266258c',
                'Volgnr'             => '3',
                'Vendor'             => 'HP',
                'ProdId'             => 'B5L54A#B19',
                'original_ProdLevId' => 'CO-HPB5L54A-B19',
                'original_supp'      => 'CO',
                'Quant'              => '1',
                'discount'           => null,
                'EUprice'            => '1771.00',
                'ShippingCosts'      => '0.00',
                'supplier'           => 'AC',
                'ProdLevId'          => '2436725',
                'Visible'            => 'N',
                'Stock'              => '5',
                'pprice'             => '1855.410',
                'original_pprice'    => '1734.59',
                'EOL'                => '0',
                'HPE'                => '0',
                'name'               => 'Actebis',
                'is_broker'          => 'false',
                'rebate_perc'        => '0.00',
                'official_distri'    => 'false',
                'oc'                 => '4.50',
                'sc'                 => '0.00',
                'treshhold'          => '10.00',
            ],
            [
                'order_id'           => '91266258',
                'Ordernumber'        => '91266258c',
                'Volgnr'             => '3',
                'Vendor'             => 'HP',
                'ProdId'             => 'B5L54A#B19',
                'original_ProdLevId' => 'CO-HPB5L54A-B19',
                'original_supp'      => 'CO',
                'Quant'              => '1',
                'discount'           => null,
                'EUprice'            => '1771.00',
                'ShippingCosts'      => '0.00',
                'supplier'           => 'CO',
                'ProdLevId'          => 'CO-HPB5L54A-B19',
                'Visible'            => 'N',
                'Stock'              => '5',
                'pprice'             => '1734.590',
                'original_pprice'    => '1734.59',
                'EOL'                => '0',
                'HPE'                => '0',
                'name'               => 'Copaco',
                'is_broker'          => 'false',
                'rebate_perc'        => '0.70',
                'official_distri'    => 'true',
                'oc'                 => '8.40',
                'sc'                 => '0.00',
                'treshhold'          => '0.00',
            ],
            [
                'order_id'           => '91266258',
                'Ordernumber'        => '91266258c',
                'Volgnr'             => '3',
                'Vendor'             => 'HP',
                'ProdId'             => 'B5L54A#B19',
                'original_ProdLevId' => 'CO-HPB5L54A-B19',
                'original_supp'      => 'CO',
                'Quant'              => '1',
                'discount'           => null,
                'EUprice'            => '1771.00',
                'ShippingCosts'      => '0.00',
                'supplier'           => 'IM',
                'ProdLevId'          => '3023070',
                'Visible'            => 'N',
                'Stock'              => '0',
                'pprice'             => '1742.320',
                'original_pprice'    => '1734.59',
                'EOL'                => '0',
                'HPE'                => '0',
                'name'               => 'Ingram Micro',
                'is_broker'          => 'false',
                'rebate_perc'        => '0.70',
                'official_distri'    => 'true',
                'oc'                 => '8.00',
                'sc'                 => '0.00',
                'treshhold'          => '0.00',
            ],
            [
                'order_id'           => '91266258',
                'Ordernumber'        => '91266258c',
                'Volgnr'             => '3',
                'Vendor'             => 'HP',
                'ProdId'             => 'B5L54A#B19',
                'original_ProdLevId' => 'CO-HPB5L54A-B19',
                'original_supp'      => 'CO',
                'Quant'              => '1',
                'discount'           => null,
                'EUprice'            => '1771.00',
                'ShippingCosts'      => '0.00',
                'supplier'           => 'TD',
                'ProdLevId'          => '3529885',
                'Visible'            => 'N',
                'Stock'              => '0',
                'pprice'             => '1724.900',
                'original_pprice'    => '1734.59',
                'EOL'                => '0',
                'HPE'                => '0',
                'name'               => 'TechData',
                'is_broker'          => 'false',
                'rebate_perc'        => '0.40',
                'official_distri'    => 'true',
                'oc'                 => '7.95',
                'sc'                 => '0.00',
                'treshhold'          => '0.00',
            ],
            [
                'order_id'           => '91266258',
                'Ordernumber'        => '91266258c',
                'Volgnr'             => '3',
                'Vendor'             => 'HP',
                'ProdId'             => 'B5L54A#B19',
                'original_ProdLevId' => 'CO-HPB5L54A-B19',
                'original_supp'      => 'CO',
                'Quant'              => '1',
                'discount'           => null,
                'EUprice'            => '1771.00',
                'ShippingCosts'      => '0.00',
                'supplier'           => 'TV',
                'ProdLevId'          => 'HPB5L54A',
                'Visible'            => 'Y',
                'Stock'              => '2',
                'pprice'             => '1532.630',
                'original_pprice'    => '1734.59',
                'EOL'                => '0',
                'HPE'                => '0',
                'name'               => 'Travion',
                'is_broker'          => 'true',
                'rebate_perc'        => '0.37',
                'official_distri'    => 'false',
                'oc'                 => '7.75',
                'sc'                 => '0.00',
                'treshhold'          => '0.00',
            ],
        ];
    }

    private function mockCriteria()
    {
        $mocks = [
            $this->getMockBuilder('components\suggestions\criteria\SuppliersCountCriteria')
                ->setMockClassName('SuppliersCountCriteria')
                ->getMock(),
            $this->getMockBuilder('components\suggestions\criteria\BrokerCriteria')
                ->setMockClassName('BrokerCriteria')
                ->getMock(),
            $this->getMockBuilder('components\suggestions\criteria\SuppliersCostCriteria')
                ->setMockClassName('SuppliersCostCriteria')
                ->getMock(),
        ];

        $mocks[0]->method('calcCriteria')->willReturn(1);
        $mocks[1]->method('calcCriteria')->willReturn(1);
        $mocks[2]->method('calcCriteria')->willReturn(1);

        $mocks[0]->method('getConfig')->willReturn(['prio' => 1]);
        $mocks[1]->method('getConfig')->willReturn(['prio' => 0.9]);
        $mocks[2]->method('getConfig')->willReturn(['prio' => 0.8]);

        $mocks[0]->method('apply')->willReturn($this->suggestions);
        $mocks[1]->method('apply')->willReturn($this->suggestions);
        $mocks[2]->method('apply')->willReturn($this->suggestions);

        $this->criteriaMock = $mocks;
    }

    public function _before()
    {
        $this->mockProducts();
        $this->mockDb();
        $this->mockSuggestions();
        $this->mockCriteria();
    }

    /**
     * Assures exception is thrown when all keys are empty.
     *
     * @return void
     */
    public function testAssureThrowsExceptionOnBadArgs()
    {
        $this->expectException('TypeError');
        (new Suggestions(null, ['foo', 'bar']));
    }

    /**
     * Assures exception is thrown on empty or incorrect order_id.
     *
     * @return void
     */
    public function testAssureThrowsExceptionOnBadOrderId()
    {
        $this->expectException('BadMethodCallException');
        (new Suggestions($this->dbMock, $this->criteriaMock))
            ->setOrder('foobar')
            ->get();
    }

    /**
     * Assures exception is thrown on empty or incorrect products.
     *
     * @return void
     */
    public function testAssureThrowsExceptionOnBadProducts()
    {
        $this->expectException('BadMethodCallException');
        (new Suggestions($this->dbMock, $this->criteriaMock))
            ->setProducts(['foobar'])
            ->get();
    }

    public function testSuggestions()
    {
        $s = (new Suggestions($this->dbMock, $this->criteriaMock))
            ->setOrder(91266060)
            ->get();

        $suggestionsExpected = [
            [
                'lines'     =>
                    [
                        [
                            'order_id'    => '91266258',
                            'Ordernumber' => '91266258u',
                            'Volgnr'      => '1',
                            'Supplier'    => 'AC',
                            'ProdLevId'   => '2528276',
                            'ProdId'      => 'J9A91EA#ABB',
                            'Vendor'      => 'HP',
                            'Stock'       => '0',
                            'Quant'       => '1',
                            'Pprice'      => '325.730',
                            'discount'    => 0,
                            'EUprice'     => '347.00',
                            'purchasing'  => 325.73,
                        ],
                        [
                            'order_id'    => '91266258',
                            'Ordernumber' => '91266258u',
                            'Volgnr'      => '1',
                            'Supplier'    => 'TD',
                            'ProdLevId'   => '3740243',
                            'ProdId'      => 'J9A91EA#ABB',
                            'Vendor'      => 'HP',
                            'Stock'       => '25',
                            'Quant'       => '1',
                            'Pprice'      => '322.150',
                            'discount'    => 0,
                            'EUprice'     => '347.00',
                            'purchasing'  => 322.15,
                        ],
                        [
                            'order_id'    => '91266258',
                            'Ordernumber' => '91266258c',
                            'Volgnr'      => '2',
                            'Supplier'    => 'CO',
                            'ProdLevId'   => 'CO-HPB3G86A-B19',
                            'ProdId'      => 'B3G86A#B19',
                            'Vendor'      => 'HP',
                            'Stock'       => '1',
                            'Quant'       => '1',
                            'Pprice'      => '3476.130',
                            'discount'    => 0,
                            'EUprice'     => '3656.00',
                            'purchasing'  => 3476.13,
                        ],
                    ],
                'costsData' =>
                    [
                        'costPlus1' => 230.76,
                        'costPlus2' => 5.6,
                        'totalCost' => 4119.24,
                        'suppliers' =>
                            [
                                [
                                    'name'            => 'Actebis',
                                    'is_broker'       => 'false',
                                    'code'            => 'AC',
                                    'oc'              => '4.50',
                                    'sc'              => '0.00',
                                    'treshhold'       => '10.00',
                                    'rebate_perc'     => '0.00',
                                    'quantity'        => '1',
                                    'official_distri' => 'false',
                                    'purchasing'      => 325.73,
                                    'total'           => 330.23,
                                ],
                                [
                                    'name'            => 'TechData',
                                    'is_broker'       => 'false',
                                    'code'            => 'TD',
                                    'oc'              => '7.95',
                                    'sc'              => '0.00',
                                    'treshhold'       => '0.00',
                                    'rebate_perc'     => '0.40',
                                    'quantity'        => '1',
                                    'official_distri' => 'true',
                                    'purchasing'      => 322.15,
                                    'total'           => 328.81,
                                ],
                                [
                                    'name'            => 'Copaco',
                                    'is_broker'       => 'false',
                                    'code'            => 'CO',
                                    'oc'              => '8.40',
                                    'sc'              => '0.00',
                                    'treshhold'       => '0.00',
                                    'rebate_perc'     => '0.70',
                                    'quantity'        => '1',
                                    'official_distri' => 'true',
                                    'purchasing'      => 3476.13,
                                    'total'           => 3460.2,
                                ],
                            ],
                    ],
                'criteria'  =>
                    [
                    ],
            ],
            [
                'lines'     =>
                    [
                        [
                            'order_id'    => '91266258',
                            'Ordernumber' => '91266258u',
                            'Volgnr'      => '1',
                            'Supplier'    => 'TV',
                            'ProdLevId'   => 'HPJ9A91EA',
                            'ProdId'      => 'J9A91EA#ABB',
                            'Vendor'      => 'HP',
                            'Stock'       => '0',
                            'Quant'       => '1',
                            'Pprice'      => '321.600',
                            'discount'    => 0,
                            'EUprice'     => '347.00',
                            'purchasing'  => 321.6,
                        ],
                        [
                            'order_id'    => '91266258',
                            'Ordernumber' => '91266258u',
                            'Volgnr'      => '1',
                            'Supplier'    => 'TD',
                            'ProdLevId'   => '3740243',
                            'ProdId'      => 'J9A91EA#ABB',
                            'Vendor'      => 'HP',
                            'Stock'       => '25',
                            'Quant'       => '1',
                            'Pprice'      => '322.150',
                            'discount'    => 0,
                            'EUprice'     => '347.00',
                            'purchasing'  => 322.15,
                        ],
                        [
                            'order_id'    => '91266258',
                            'Ordernumber' => '91266258c',
                            'Volgnr'      => '2',
                            'Supplier'    => 'CO',
                            'ProdLevId'   => 'CO-HPB3G86A-B19',
                            'ProdId'      => 'B3G86A#B19',
                            'Vendor'      => 'HP',
                            'Stock'       => '1',
                            'Quant'       => '1',
                            'Pprice'      => '3476.130',
                            'discount'    => 0,
                            'EUprice'     => '3656.00',
                            'purchasing'  => 3476.13,
                        ],
                    ],
                'costsData' =>
                    [
                        'costPlus1' => 232.83,
                        'costPlus2' => 5.66,
                        'totalCost' => 4117.17,
                        'suppliers' =>
                            [
                                [
                                    'name'            => 'Travion',
                                    'is_broker'       => 'true',
                                    'code'            => 'TV',
                                    'oc'              => '7.75',
                                    'sc'              => '0.00',
                                    'treshhold'       => '0.00',
                                    'rebate_perc'     => '0.37',
                                    'quantity'        => '1',
                                    'official_distri' => 'false',
                                    'purchasing'      => 321.60,
                                    'total'           => 328.16,
                                ],
                                [
                                    'name'            => 'TechData',
                                    'is_broker'       => 'false',
                                    'code'            => 'TD',
                                    'oc'              => '7.95',
                                    'sc'              => '0.00',
                                    'treshhold'       => '0.00',
                                    'rebate_perc'     => '0.40',
                                    'quantity'        => '1',
                                    'official_distri' => 'true',
                                    'purchasing'      => 322.15,
                                    'total'           => 328.81,
                                ],
                                [
                                    'name'            => 'Copaco',
                                    'is_broker'       => 'false',
                                    'code'            => 'CO',
                                    'oc'              => '8.40',
                                    'sc'              => '0.00',
                                    'treshhold'       => '0.00',
                                    'rebate_perc'     => '0.70',
                                    'quantity'        => '1',
                                    'official_distri' => 'true',
                                    'purchasing'      => 3476.13,
                                    'total'           => 3460.2,
                                ],
                            ],
                    ],
                'criteria'  =>
                    [
                    ],
            ],
            [
                'lines'     =>
                    [
                        [
                            'order_id'    => '91266258',
                            'Ordernumber' => '91266258u',
                            'Volgnr'      => '1',
                            'Supplier'    => 'CO',
                            'ProdLevId'   => 'CO-HPJ9A91EA-ABB',
                            'ProdId'      => 'J9A91EA#ABB',
                            'Vendor'      => 'HP',
                            'Stock'       => '1',
                            'Quant'       => '1',
                            'Pprice'      => '321.180',
                            'discount'    => 0,
                            'EUprice'     => '347.00',
                            'purchasing'  => 321.18,
                        ],
                        [
                            'order_id'    => '91266258',
                            'Ordernumber' => '91266258u',
                            'Volgnr'      => '1',
                            'Supplier'    => 'IM',
                            'ProdLevId'   => '3142857',
                            'ProdId'      => 'J9A91EA#ABB',
                            'Vendor'      => 'HP',
                            'Stock'       => '0',
                            'Quant'       => '1',
                            'Pprice'      => '321.990',
                            'discount'    => 0,
                            'EUprice'     => '347.00',
                            'purchasing'  => 321.99,
                        ],
                        [
                            'order_id'    => '91266258',
                            'Ordernumber' => '91266258u',
                            'Volgnr'      => '1',
                            'Supplier'    => 'TV',
                            'ProdLevId'   => 'HPJ9A91EA',
                            'ProdId'      => 'J9A91EA#ABB',
                            'Vendor'      => 'HP',
                            'Stock'       => '0',
                            'Quant'       => '1',
                            'Pprice'      => '321.600',
                            'discount'    => 0,
                            'EUprice'     => '347.00',
                            'purchasing'  => 321.6,
                        ],
                    ],
                'costsData' =>
                    [
                        'costPlus1' => 57.77,
                        'costPlus2' => 5.88,
                        'totalCost' => 983.23,
                        'suppliers' =>
                            [
                                [
                                    'name'            => 'Copaco',
                                    'is_broker'       => 'false',
                                    'code'            => 'CO',
                                    'oc'              => '8.40',
                                    'sc'              => '0.00',
                                    'treshhold'       => '0.00',
                                    'rebate_perc'     => '0.70',
                                    'quantity'        => '1',
                                    'official_distri' => 'true',
                                    'purchasing'      => 321.18,
                                    'total'           => 327.33,
                                ],
                                [
                                    'name'            => 'Ingram Micro',
                                    'is_broker'       => 'false',
                                    'code'            => 'IM',
                                    'oc'              => '8.00',
                                    'sc'              => '0.00',
                                    'treshhold'       => '0.00',
                                    'rebate_perc'     => '0.70',
                                    'quantity'        => '1',
                                    'official_distri' => 'true',
                                    'purchasing'      => 321.99,
                                    'total'           => 327.74,
                                ],
                                [
                                    'name'            => 'Travion',
                                    'is_broker'       => 'true',
                                    'code'            => 'TV',
                                    'oc'              => '7.75',
                                    'sc'              => '0.00',
                                    'treshhold'       => '0.00',
                                    'rebate_perc'     => '0.37',
                                    'quantity'        => '1',
                                    'official_distri' => 'false',
                                    'purchasing'      => 321.6,
                                    'total'           => 328.16,
                                ],
                            ],
                    ],
                'criteria'  =>
                    [
                    ],
            ],
            [
                'lines'     =>
                    [
                        [
                            'order_id'    => '91266258',
                            'Ordernumber' => '91266258u',
                            'Volgnr'      => '1',
                            'Supplier'    => 'TD',
                            'ProdLevId'   => '3740243',
                            'ProdId'      => 'J9A91EA#ABB',
                            'Vendor'      => 'HP',
                            'Stock'       => '25',
                            'Quant'       => '1',
                            'Pprice'      => '322.150',
                            'discount'    => 0,
                            'EUprice'     => '347.00',
                            'purchasing'  => 322.15,
                        ],
                        [
                            'order_id'    => '91266258',
                            'Ordernumber' => '91266258u',
                            'Volgnr'      => '1',
                            'Supplier'    => 'CO',
                            'ProdLevId'   => 'CO-HPJ9A91EA-ABB',
                            'ProdId'      => 'J9A91EA#ABB',
                            'Vendor'      => 'HP',
                            'Stock'       => '1',
                            'Quant'       => '1',
                            'Pprice'      => '321.180',
                            'discount'    => 0,
                            'EUprice'     => '347.00',
                            'purchasing'  => 321.18,
                        ],
                        [
                            'order_id'    => '91266258',
                            'Ordernumber' => '91266258c',
                            'Volgnr'      => '2',
                            'Supplier'    => 'IM',
                            'ProdLevId'   => '2724128',
                            'ProdId'      => 'B3G86A#B19',
                            'Vendor'      => 'HP',
                            'Stock'       => '0',
                            'Quant'       => '1',
                            'Pprice'      => '3511.110',
                            'discount'    => 0,
                            'EUprice'     => '3656.00',
                            'purchasing'  => 3511.11,
                        ],
                    ],
                'costsData' =>
                    [
                        'costPlus1' => 199.33,
                        'costPlus2' => 4.8,
                        'totalCost' => 4150.67,
                        'suppliers' =>
                            [
                                [
                                    'name'            => 'TechData',
                                    'is_broker'       => 'false',
                                    'code'            => 'TD',
                                    'oc'              => '7.95',
                                    'sc'              => '0.00',
                                    'treshhold'       => '0.00',
                                    'rebate_perc'     => '0.40',
                                    'quantity'        => '1',
                                    'official_distri' => 'true',
                                    'purchasing'      => 322.15,
                                    'total'           => 328.81,
                                ],
                                [
                                    'name'            => 'Copaco',
                                    'is_broker'       => 'false',
                                    'code'            => 'CO',
                                    'oc'              => '8.40',
                                    'sc'              => '0.00',
                                    'treshhold'       => '0.00',
                                    'rebate_perc'     => '0.70',
                                    'quantity'        => '1',
                                    'official_distri' => 'true',
                                    'purchasing'      => 321.18,
                                    'total'           => 327.33,
                                ],
                                [
                                    'name'            => 'Ingram Micro',
                                    'is_broker'       => 'false',
                                    'code'            => 'IM',
                                    'oc'              => '8.00',
                                    'sc'              => '0.00',
                                    'treshhold'       => '0.00',
                                    'rebate_perc'     => '0.70',
                                    'quantity'        => '1',
                                    'official_distri' => 'true',
                                    'purchasing'      => 3511.11,
                                    'total'           => 3494.53,
                                ],
                            ],
                    ],
                'criteria'  =>
                    [
                    ],
            ],
        ];

        $this->assertEquals($suggestionsExpected, $s);
    }

    public function testCombinationsMaking()
    {
        $data = [[4], [2, 3], [5, 7]];

        $res = CombinationsGenerator::makeCombinations($data);
        $this->assertEquals([[4, 2, 5], [4, 3, 5], [4, 2, 7], [4, 3, 7]], $res);
    }

    public function testTooManyCombinationsException()
    {
        $this->expectException('Exception');
        $arr = array_fill(0, 10000, true);
        $data = [[4], $arr, $arr];
        $productsPool = [];

        (new CombinationsGenerator($productsPool))
            ->checkMaxCombinations($data);
    }

    public function testSuppliersCountCriteria()
    {
        $criteria = new SuppliersCountCriteria();
        $criteria->setConfig([
            'name'   => 'suppliersCount',
            'prio'   => 1,
            'filter' => [
                'condition' => 'min',
                'limit'     => 1
            ]
        ]);

        $combinationsOfProducts = (new CombinationsGenerator($this->productsPool))->get();
        $result = $criteria->apply($combinationsOfProducts, $this->productsPool);

        $this->assertEquals([
            6 =>
                [
                    'products' =>
                        [
                            0 => 1,
                            1 => 9,
                            2 => 11,
                        ],
                    'criteria' =>
                        [
                            'suppliersCount' => 1,
                        ],
                ],
        ], $result);
    }

    public function testBrokerCriteria()
    {
        $criteria = new BrokerCriteria();
        $criteria->setConfig([
            'name'   => 'brokersCount',
            'prio'   => 0.9,
            'filter' => [
                'condition' => 'min',
                'limit'     => 1
            ]
        ]);

        $combinationsOfProducts = (new CombinationsGenerator($this->productsPool))->get();
        $result = $criteria->apply($combinationsOfProducts, $this->productsPool);

        $this->assertEquals([
            [
                'products' =>
                    [
                        0 => 1,
                        1 => 5,
                        2 => 10,
                    ],
                'criteria' =>
                    [
                        'brokersCount' => 0,
                    ],
            ],
            [
                'products' =>
                    [
                        0 => 4,
                        1 => 5,
                        2 => 10,
                    ],
                'criteria' =>
                    [
                        'brokersCount' => 0,
                    ],
            ],
            [
                'products' =>
                    [
                        0 => 1,
                        1 => 9,
                        2 => 10,
                    ],
                'criteria' =>
                    [
                        'brokersCount' => 0,
                    ],
            ],
            [
                'products' =>
                    [
                        0 => 4,
                        1 => 9,
                        2 => 10,
                    ],
                'criteria' =>
                    [
                        'brokersCount' => 0,
                    ],
            ],
            [
                'products' =>
                    [
                        0 => 1,
                        1 => 5,
                        2 => 11,
                    ],
                'criteria' =>
                    [
                        'brokersCount' => 0,
                    ],
            ],
            [
                'products' =>
                    [
                        0 => 4,
                        1 => 5,
                        2 => 11,
                    ],
                'criteria' =>
                    [
                        'brokersCount' => 0,
                    ],
            ],
            [
                'products' =>
                    [
                        0 => 1,
                        1 => 9,
                        2 => 11,
                    ],
                'criteria' =>
                    [
                        'brokersCount' => 0,
                    ],
            ],
            [
                'products' =>
                    [
                        0 => 4,
                        1 => 9,
                        2 => 11,
                    ],
                'criteria' =>
                    [
                        'brokersCount' => 0,
                    ],
            ],
        ], $result);
    }

    public function testSuppliersCostCriteria()
    {
        $criteria = new SuppliersCostCriteria();
        $criteria->setConfig([
            'name'   => 'suppliersCost',
            'prio'   => 0.8,
            'filter' => [
                'condition' => 'min',
                'limit'     => 3
            ]
        ]);

        $combinationsOfProducts = (new CombinationsGenerator($this->productsPool))->get();
        $result = $criteria->apply($combinationsOfProducts, $this->productsPool);

        $this->assertEquals([
            6  =>
                [
                    'products' =>
                        [
                            0 => 1,
                            1 => 9,
                            2 => 11,
                        ],
                    'criteria' =>
                        [
                            'suppliersCost' => 5501.58,
                        ],
                ],
            10 =>
                [
                    'products' =>
                        [
                            0 => 1,
                            1 => 9,
                            2 => 14,
                        ],
                    'criteria' =>
                        [
                            'suppliersCost' => 5313.84,
                        ],
                ],
            11 =>
                [
                    'products' =>
                        [
                            0 => 4,
                            1 => 9,
                            2 => 14,
                        ],
                    'criteria' =>
                        [
                            'suppliersCost' => 5323.72,
                        ],
                ],
        ], $result);
    }
}
