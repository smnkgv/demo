<?php
/**
 * Created by PhpStorm.
 * User: gleb
 * Date: 23.06.16
 * Time: 10:47
 */

namespace components\suggestions;

use components\suggestions\criteria\CriteriaInterface;
use components\Totals;
use yii\db\Connection;

/**
 * Class Suggestions
 * Allows to generate suggestions how order can be transformed by changing some suppliers
 *
 * @package app\components
 */
class Suggestions
{
    
    /**
     * Instance of db
     *
     * @var \yii\db\Connection
     */
    private $db;

    /**
     * Number of the order
     *
     * @var integer
     */
    private $orderId;

    /**
     * Array of conditions of products
     *
     * @var array
     */
    private $productsConditions = [];

    /**
     * Array of all suggestins
     *
     * @var array
     */
    private $suggestions = [];

    /**
     * Config with filtering criteria in format: method name, prio, get min/max value
     *
     * @var array
     */
    private $criteriaList = [];

    /**
     * Products stored in this property for better memory management.
     * In possible suggestions we use only indexes from this array.
     *
     * @var array
     */
    private $productsPool = [];

    /**
     * Config for all allowed suggestions
     */
    const CONFIG = [
        'criteria' => [
            'SuppliersCountCriteria' => [
                'name'   => 'suppliersCount',
                'prio'   => 1,
                'filter' => [
                    'condition' => 'min',
                    'limit'     => 1
                ]
            ],
            'BrokerCriteria'         => [
                'name'   => 'brokersCount',
                'prio'   => 0.9,
                'filter' => [
                    'condition' => 'min',
                    'limit'     => 1
                ]
            ],
            'SuppliersCostCriteria'  => [
                'name'   => 'suppliersCost',
                'prio'   => 0.8,
                'filter' => [
                    'condition' => 'min',
                    'limit'     => 5,
                ]
            ],
        ],
        'sort'     => [
            'criterion' => 'suppliersCost',
            'order'     => 'asc',
        ]
    ];

    /**
     * Suggestions constructor.
     *
     * @param \yii\db\Connection $db
     * @param array $criteria
     */
    public function __construct(Connection $db, array $criteria)
    {
        $this->db = $db;
        $this->setCriteria($criteria);
    }

    /**
     * Allows to set one criteria
     *
     * @param CriteriaInterface $criterion
     * @return $this
     * @throws \Exception
     * @internal param CriteriaInterface $criretion
     */
    public function setCriterion(CriteriaInterface $criterion)
    {
        $criterionClassNameExploded = explode('\\', get_class($criterion));
        $criterionClassName = end($criterionClassNameExploded);

        if (empty(self::CONFIG['criteria'][$criterionClassName])) {
            throw new \BadMethodCallException('There is no config for such criterion');
        }

        foreach ($this->criteriaList as $item) {
            if (get_class($item) == get_class($criterion)) {
                throw new \BadMethodCallException('Trying to set the same criterion twice');
            }
        }

        $criterion->setConfig(self::CONFIG['criteria'][$criterionClassName]);

        $this->criteriaList[] = $criterion;

        usort($this->criteriaList, function ($a, $b) {
            return ($a->getConfig()['prio'] > $b->getConfig()['prio']) ? -1 : 1;
        });

        return $this;
    }

    /**
     * Allows to set an array of criteria
     *
     * @param array $criteria
     * @return $this
     * @throws \Exception
     */
    public function setCriteria(array $criteria)
    {
        foreach ($criteria as $criterion) {
            $this->setCriterion($criterion);
        }

        return $this;
    }

    /**
     * Allows to set list of products as pk. Keys ProdId and Vendor required.
     *
     * @param array $productsConditions
     * @return $this
     * @throws \BadMethodCallException
     */
    public function setProducts(array $productsConditions)
    {
        if (empty($productsConditions)) {
            throw new \BadMethodCallException('List of products can not be empty');
        }

        foreach ($productsConditions as $productsCondition) {
            if (empty($productsCondition['ProdId']) || empty($productsCondition['Vendor'])) {
                throw new \BadMethodCallException('List of products is incorrect');
            }
        }

        $this->productsConditions = $productsConditions;

        return $this;
    }

    /**
     * Allows to set order_id as pk.
     *
     * @param $orderId
     * @return $this
     * @throws \BadMethodCallException
     */
    public function setOrder($orderId)
    {
        if (abs((int) $orderId) != $orderId) {
            throw new \BadMethodCallException('order_id is incorrect');
        }

        $this->orderId = (int) $orderId;

        return $this;
    }

    /**
     * Main method. Allows to get suggestions.
     *
     * @return array
     * @throws \BadMethodCallException
     */
    public function get()
    {
        if (empty($this->orderId) && empty($this->productsConditions)) {
            throw new \BadMethodCallException('All possible keys are empty');
        }

        $this->productsPool = $this->getPossibleProducts();

        if (empty($this->productsPool)) {
            return [];
        }

        $this->suggestions = $this->getSuggestions($this->criteriaList, $this->productsPool);
        $suggestionsWithProducts = $this->getSuggestionsWithData($this->suggestions, $this->productsPool);

        return $suggestionsWithProducts;
    }

    /**
     * Allows to get list of suggestion, keys of products only
     *
     * @param array $criteriaList
     * @param array $productPool
     * @return array
     */
    private function getSuggestions(array $criteriaList, array &$productPool)
    {
        $combinationsOfProducts = (new CombinationsGenerator($productPool))->get();
        $suggestions = $this->applyCriteria($criteriaList, $combinationsOfProducts, $productPool);
        $suggestions = $this->removeCurrentCombination($suggestions, $productPool);
        $suggestions = $this->sort($suggestions);

        return $suggestions;
    }

    /**
     * Allows to get list of suggestion in human-readable format
     *
     * @param array $suggestions
     * @param array $productsPool
     * @return array
     */
    private function getSuggestionsWithData(array $suggestions, array &$productsPool)
    {
        $suggestionsResult = [];

        foreach ($suggestions as $suggestion) {
            $orders = [];
            $lines = [];
            $suppliers = [];
            foreach ($suggestion['products'] as $index) {
                $product = $productsPool[$index];

                $orders[$product['Ordernumber']] = [
                    'ShippingCosts' => $product['ShippingCosts']
                ];

                $lineSupplierCode = strtoupper($product['supplier'] ?: $product['original_supp']);
                $pprice = $product['pprice'] ?: $product['original_pprice'];

                $lines[] = [
                    'order_id'    => $product['order_id'],
                    'Ordernumber' => $product['Ordernumber'],
                    'Volgnr'      => $product['Volgnr'],
                    'Supplier'    => $lineSupplierCode,
                    'ProdLevId'   => $product['ProdLevId'] ?: $product['original_ProdLevId'],
                    'ProdId'      => $product['ProdId'],
                    'Vendor'      => $product['Vendor'],
                    'Stock'       => $product['Stock'] ?: 0,
                    'Quant'       => $product['Quant'],
                    'Pprice'      => $pprice,
                    'discount'    => $product['discount'] ?: 0,
                    'EUprice'     => $product['EUprice'],
                    'purchasing'  => round($pprice * $product['Quant'], 2)
                ];

                $suppliers[$lineSupplierCode] = [
                    'name'        => $product['name'],
                    'is_broker'   => $product['is_broker'],
                    'code'        => $lineSupplierCode,
                    'oc'          => $product['oc'],
                    'sc'          => $product['sc'],
                    'treshhold'   => $product['treshhold'],
                    'rebate_perc' => $product['rebate_perc'],
                    'quantity'    => isset($suppliers[$lineSupplierCode]['quantity'])
                        ? $suppliers[$lineSupplierCode]['quantity'] + $product['Quant']
                        : $product['Quant'],
                    'official_distri' => $product['official_distri']
                ];
            }

            $totals = new Totals();
            $totalsBySuppliers = $totals->calcTotalsBySuppliers($lines, $suppliers);
            $purchasingBySuppliers = $totals->calcPurchasingBySuppliers($lines);

            foreach ($suppliers as &$supplier) {
                $supplierCode = strtoupper($supplier['code']);
                $supplier['purchasing'] = $purchasingBySuppliers[$supplierCode];
                $supplier['total'] = $totalsBySuppliers[$supplierCode];
            }
            $suppliers = array_values($suppliers);

            $costs = [
                'costPlus1' => $totals->calcCostPlus($lines, $suppliers, $orders),
                'costPlus2' => $totals->calcCostPlusInPercents($lines, $suppliers, $orders),
                'totalCost' => $totals->calcTotalsSumBySuppliers($lines, $suppliers),
                'suppliers' => $suppliers,
            ];

            $suggestionsResult[] = [
                'lines' => $lines,
                'costsData' => $costs,
                'criteria' => isset($suggestion['criteria']) ? $suggestion['criteria'] : [],
            ];
        }

        return $suggestionsResult;
    }

    /**
     * Remove suggestion which has the same combination of products which is selected in the current order
     *
     * @param array $suggestions
     * @param array $productsPool
     * @return array
     */
    private function removeCurrentCombination(array $suggestions, array &$productsPool)
    {
        $currentCombination = array_filter($suggestions, function ($suggestion) use (&$productsPool) {
            foreach ($suggestion['products'] as $index) {
                $product = $productsPool[$index];

                if ($product['original_ProdLevId'] != $product['ProdLevId']
                    || $product['original_supp'] != $product['supplier']
                ) {
                    return false;
                }
            }

            return true;
        });

        unset($suggestions[key($currentCombination)]);

        return $suggestions;
    }

    /**
     * Allows to sort suggestions by some criterion
     *
     * @param array $suggestions
     * @return array
     */
    private function sort(array $suggestions)
    {
        foreach ($suggestions as $suggestion) {
            if (empty($suggestion['criteria'][self::CONFIG['sort']['criterion']])) {
                return $suggestions;
            }
        }

        usort($suggestions, function ($a, $b) {
            $aCriterion = $a['criteria'][self::CONFIG['sort']['criterion']];
            $bCriterion = $b['criteria'][self::CONFIG['sort']['criterion']];

            if ($aCriterion === $bCriterion) {
                return 0;
            }

            return $aCriterion > $bCriterion ? 1 : -1;
        });

        if (self::CONFIG['sort']['order'] === 'desc') {
            $suggestions = array_reverse($suggestions);
        }

        return $suggestions;
    }

    /**
     * Getting all possible products from db
     *
     * @return array|bool
     */
    private function getPossibleProducts()
    {
        if (empty($this->orderId)) {
            return false;
        }

        return $this->db->createCommand("
            SELECT
              xxx
            FROM
              xxx
            JOIN xxx 
            LEFT JOIN xxx
            LEFT JOIN xxx
            LEFT JOIN xxx
            WHERE
              xxx = :orderId
            ORDER BY
              xxx ASC
        ", [
            ':orderId' => $this->orderId
        ])->queryAll();
    }

    /**
     * Main method which applies criteria
     *
     * @param array $criteriaList
     * @param array $suggestions
     * @param array $productsPool
     * @return array
     */
    private function applyCriteria(array $criteriaList, array $suggestions, array &$productsPool)
    {
        $suggestionsFiltered = $suggestions;
        foreach ($criteriaList as $criterion) {
            if (count($suggestionsFiltered) > 1) {
                $suggestionsFiltered = $criterion->apply($suggestionsFiltered, $productsPool);
            }
        }

        return $suggestionsFiltered;
    }
}
