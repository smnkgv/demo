<?php
/**
 * Created by PhpStorm.
 * User: gleb
 * Date: 06.07.16
 * Time: 14:12
 */

namespace components\suggestions;

/**
 * Class CombinationsGenerator
 * Generates all possible combinations of products for all order lines
 * 
 * @package app\components\suggestions
 */
class CombinationsGenerator
{
    const MAX_COMBINATIONS_COUNT = 1000000;

    private $productsPool;

    /**
     * CombibationsGenerator constructor.
     *
     * @param array $products
     */
    public function __construct(array &$products)
    {
        $this->productsPool = &$products;
    }

    /**
     * Get all possible combinations of the suppliers.
     * Kinda like not filtered raw suggestions
     *
     * @return array $suggestions
     */
    public function get()
    {
        if (empty($this->productsPool)) {
            throw new \BadMethodCallException('Products are empty');
        }

        $productsKeys = array_keys($this->productsPool);
        $productsByVolgnr = $this->groupByVolgnr($productsKeys);
        $productsByVolgnr = $this->setFixedProducts($productsByVolgnr);
        $productsByVolgnr = $this->filterOutOfStock($productsByVolgnr);

        $this->checkMaxCombinations($productsByVolgnr);
        $combinations = self::makeCombinations(array_values($productsByVolgnr));

        $suggestions = array_map(function ($value) {
            return [
                'products' => $value
            ];
        }, $combinations);

        return $suggestions;
    }

    /**
     * Allows to group raw products list by Volgnr
     *
     * @param array $productsIndexes
     * @return mixed
     */
    private function groupByVolgnr(array $productsIndexes)
    {
        $products = array_reduce($productsIndexes, function ($acc, $index) {
            $product = $this->productsPool[$index];
            $acc[$product['Volgnr']][] = $index;
            return $acc;
        }, []);
        return $products;
    }

    /**
     * If we have EOL or HBE product we'll left only this product in group
     *
     * @param array $productsByVolgnr
     * @return mixed
     */
    private function setFixedProducts(array $productsByVolgnr)
    {
        $productsByVolgnr = array_map(function ($products) {
            foreach ($products as $index) {
                $product = $this->productsPool[$index];
                if ($product['HPE'] || $product['EOL']) {
                    $product['supplier'] = $product['original_supp'];
                    $product['ProdLevId'] = $product['original_ProdLevId'];
                    $this->productsPool[$index] = $product;

                    return [$index];
                }
            }

            return $products;
        }, $productsByVolgnr);

        return $productsByVolgnr;
    }

    /**
     * Remove products which are out of stock
     *
     * @param array $productsByVolgnr
     * @return array
     */
    private function filterOutOfStock(array $productsByVolgnr)
    {
        foreach ($productsByVolgnr as &$products) {
            $products = array_filter($products, function ($index) {
                $product = $this->productsPool[$index];
                if (!empty($product['supplier'])
                    && $product['Stock'] == 0
                    && $product['supplier'] != $product['original_supp']
                ) {
                    return false;
                }

                return true;
            });
        }

        return $productsByVolgnr;
    }

    /**
     * Check if we have enough resources to calculate all possible suggestion variants
     *
     * @param $products
     * @throws \Exception
     */
    public function checkMaxCombinations($products)
    {
        $count = array_reduce($products, function ($acc, $group) {
            $acc *= count($group);
            return $acc;
        }, 1);

        if ($count > self::MAX_COMBINATIONS_COUNT) {
            throw new \Exception('Heck, too many variants.');
        }
    }

    /**
     * Recursive function which returns resulting set of permutations
     * @param $array
     * @param array $acc
     * @param int $group
     * @return array
     */
    public static function makeCombinations($array, $acc = [], $group = 0)
    {
        if ($group >= count($array)) {
            return $acc;
        }

        $storage = [];
        foreach ($array[$group] as $i) {
            if (empty($acc)) {
                $storage[] = [$i];
            }
            
            foreach ($acc as $item) {
                $storage[] = array_merge($item, [$i]);
            }
        }
        
        $group++;
        return self::makeCombinations($array, $storage, $group);
    }
}
