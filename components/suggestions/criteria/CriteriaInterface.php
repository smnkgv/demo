<?php
/**
 * Created by PhpStorm.
 * User: gleb
 * Date: 06.07.16
 * Time: 10:11
 */

namespace components\suggestions\criteria;

/**
 * Interface CriteriaInterface
 * Defines which methods must be implemented in all criteria classes
 * 
 * @package app\components\suggestions\criteria
 */
interface CriteriaInterface
{
    /**
     * Method which allows to calc criteria (value only) for single suggestion
     *
     * @param array $suggestion
     * @param $productsPool
     * @return integer
     */
    public function calcCriteria(array $suggestion, &$productsPool);
}
