<?php
/**
 * Created by PhpStorm.
 * User: gleb
 * Date: 08.07.16
 * Time: 11:08
 */

namespace components\suggestions\criteria;

/**
 * Class AbstractCriteria
 * Base class for all criteria
 *
 * @package app\components\suggestions\criteria
 */
abstract class AbstractCriteria
{
    protected $config = [];

    /**
     * Allows to set criterion config
     *
     * @param array $config
     * @return $this
     */
    public function setConfig(array $config)
    {
        if (empty($config)) {
            throw new \BadMethodCallException('Config is empty');
        }

        $this->config = $config;

        return $this;
    }

    public function getConfig()
    {
        return $this->config;
    }

    /**
     * Main method which allows to calc criterion and then filter all the suggestions
     *
     * @param array $suggestions
     * @param array $productsPool
     * @return mixed
     */
    public function apply(array $suggestions, array &$productsPool)
    {
        if (empty($this->config)) {
            throw new \BadMethodCallException('Config is empty');
        }

        $criteriaResult = [];
        foreach ($suggestions as $key => $suggestion) {
            $criteriaResult[$key] = $this->calcCriteria($suggestion, $productsPool);
        }

        $suggestions = $this->filter($criteriaResult, $suggestions, $productsPool);
        $suggestions = $this->appendCriteria($criteriaResult, $suggestions);

        return $suggestions;
    }

    /**
     * Filters all suggestions by some criterion
     *
     * @param array $criteriaResult
     * @param array $suggestions
     * @param array $productsPool
     * @return mixed
     */
    protected function filter(array $criteriaResult, array $suggestions, array &$productsPool = [])
    {
        if ($this->config['filter']['condition'] === 'max') {
            arsort($criteriaResult);
        } else {
            asort($criteriaResult);
        }

        $criteriaResultUniqueValues = array_unique($criteriaResult);
        $criteriaResultLimited = array_slice($criteriaResultUniqueValues, 0, $this->config['filter']['limit']);
        $criteriaResultFiltered = array_intersect($criteriaResult, $criteriaResultLimited);
        $suggestionsFiltered = array_intersect_key($suggestions, $criteriaResultFiltered);

        return $suggestionsFiltered;
    }

    /**
     * Appends criteria results to suggestions
     *
     * @param array $criteriaResult
     * @param array $suggestions
     * @return array
     */
    private function appendCriteria(array $criteriaResult, array $suggestions)
    {
        foreach ($suggestions as $key => &$suggestion) {
            $suggestion['criteria'][$this->config['name']] = $criteriaResult[$key];
        }
        
        return $suggestions;
    }
}
