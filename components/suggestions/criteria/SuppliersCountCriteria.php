<?php
/**
 * Created by PhpStorm.
 * User: gleb
 * Date: 05.07.16
 * Time: 11:02
 */

namespace components\suggestions\criteria;

/**
 * Class SuppliersCountCriteria
 * Allows to get count of suppliers for one suggestion.
 *
 * @package app\components\Suggestions
 */
class SuppliersCountCriteria extends AbstractCriteria implements CriteriaInterface
{
    /**
     * @inheritdoc
     */
    public function calcCriteria(array $suggestion, &$productsPool)
    {
        $criteriaValues = array_reduce($suggestion['products'], function ($acc, $index) use ($productsPool) {
            $product = $productsPool[$index];
            $supplier = strtoupper($product['supplier'] ?: $product['original_supp']);
            $acc[$supplier] = true;

            return $acc;
        }, []);

        return count($criteriaValues);
    }

    /**
     * Filters all suggestions by this criterion and removes criteria if count of suppliers > count
     * of suppliers in current order.
     *
     * @param array $criteriaResult
     * @param array $suggestions
     * @param array $productsPool
     * @return mixed
     */
    protected function filter(array $criteriaResult, array $suggestions, array &$productsPool = [])
    {
        $suggestionsFiltered = parent::filter($criteriaResult, $suggestions);

        $currentSuppliers = array_unique(array_map(function ($one) {
            return strtoupper($one['original_supp']);
        }, $productsPool));

        $currentSuppliersCount = count($currentSuppliers);

        return array_filter($suggestionsFiltered, function ($key) use ($criteriaResult, $currentSuppliersCount) {
            return $criteriaResult[$key] < $currentSuppliersCount;
        }, ARRAY_FILTER_USE_KEY);
    }
}
