<?php
/**
 * Created by PhpStorm.
 * User: gleb
 * Date: 05.07.16
 * Time: 11:03
 */

namespace components\suggestions\criteria;

use components\Totals;

/**
 * Class SuppliersCostCriteria
 * Allows to get the internal price for one suggestion.
 * 
 * @package app\components\Suggestions
 */
class SuppliersCostCriteria extends AbstractCriteria implements CriteriaInterface
{
    /**
     * @inheritdoc
     */
    public function calcCriteria(array $suggestion, &$productsPool)
    {
        $orderDetails = array_reduce($suggestion['products'], function ($memo, $index) use ($productsPool) {
            $product = $productsPool[$index];
            $memo[] = [
                'Supplier' => $product['supplier'],
                'Pprice'   => $product['pprice'],
                'Quant'    => $product['Quant'],
            ];

            return $memo;
        }, []);

        $suppliersWithOc = array_reduce($suggestion['products'], function ($memo, $index) use ($productsPool) {
            $product = $productsPool[$index];
            $memo[$product['supplier']] = [
                'code'        => $product['supplier'],
                'oc'          => $product['oc'],
                'sc'          => $product['sc'],
                'treshhold'   => $product['treshhold'],
                'rebate_perc' => $product['rebate_perc'],
            ];

            return $memo;
        }, []);

        $totalsSubBySuppliers = (new Totals())->calcTotalsSumBySuppliers($orderDetails, $suppliersWithOc);

        return $totalsSubBySuppliers;
    }
}
