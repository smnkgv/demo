<?php
/**
 * Created by PhpStorm.
 * User: gleb
 * Date: 05.07.16
 * Time: 11:03
 */

namespace components\suggestions\criteria;

/**
 * Class BrokerCriteria
 * Allows to get count of brokers for one suggestion.
 * 
 * @package app\components\Suggestions
 */
class BrokerCriteria extends AbstractCriteria implements CriteriaInterface
{
    /**
     * @inheritdoc
     */
    public function calcCriteria(array $suggestion, &$productsPool)
    {
        $criteriaValues = array_reduce($suggestion['products'], function ($acc, $index) use ($productsPool) {
            $product = $productsPool[$index];
            $acc[$product['supplier']] = $product['is_broker'] === 'true' ? 1 : 0;

            return $acc;
        }, []);

        return array_sum($criteriaValues);
    }
}
