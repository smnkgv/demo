<?php
/**
 * Created by PhpStorm.
 * User: gleb
 * Date: 08.07.16
 * Time: 14:40
 */

namespace components;

/**
 * Class Totals
 * Calculates totals like cost+, subtotal, vat, etc of the orders.
 * Only logic based on php vars, no mysql queries please
 *
 * @package app\components
 */
class Totals
{
    /**
     * Calculates cost+ of the order.
     *
     * @param array $orderDetails order lines by order_id
     * @param array $suppliersWithOc info about all suppliers of the order from bo_supplier with joined bo_supplier_oc
     * @param array $orders order by order_id
     * @return number
     */
    public function calcCostPlus(array $orderDetails, array $suppliersWithOc, array $orders)
    {
        $subtotal = $this->calcSubtotal($orderDetails, $orders);
        $totalsSumBySuppliers = $this->calcTotalsSumBySuppliers($orderDetails, $suppliersWithOc);
        $costPlus = $subtotal - $totalsSumBySuppliers;

        return round($costPlus, 2);
    }

    /**
     * Calculates cost+ in % of the order
     *
     * @param array $orderDetails order lines by order_id
     * @param array $suppliersWithOc info about all suppliers of the order from bo_supplier with joined bo_supplier_oc
     * @param array $orders order by order_id
     * @return float
     */
    public function calcCostPlusInPercents(array $orderDetails, array $suppliersWithOc, array $orders)
    {
        $costPlus = $this->calcCostPlus($orderDetails, $suppliersWithOc, $orders);
        $totalsSumBySuppliers = $this->calcTotalsSumBySuppliers($orderDetails, $suppliersWithOc);

        return round($costPlus / $totalsSumBySuppliers * 100, 2);
    }

    /**
     * Calculates sum of totals of the order lines grouped by supplier
     *
     * @param array $orderDetails order lines by order_id
     * @param array $suppliersWithOc info about all suppliers of the order from bo_supplier with joined bo_supplier_oc
     * @return number
     */
    public function calcTotalsSumBySuppliers(array $orderDetails, array $suppliersWithOc)
    {
        $totalsBySuppliers = $this->calcTotalsBySuppliers($orderDetails, $suppliersWithOc);

        return array_sum($totalsBySuppliers);
    }

    /**
     * Calculates totals of the order lines grouped by supplier
     *
     * @param array $orderDetails order lines by order_id
     * @param array $suppliersWithOc info about all suppliers of the order from bo_supplier with joined bo_supplier_oc
     * @return array
     */
    public function calcTotalsBySuppliers(array $orderDetails, array $suppliersWithOc)
    {
        $purchasingBySuppliers = $this->calcPurchasingBySuppliers($orderDetails);

        $totalsBySuppliers = array_reduce($suppliersWithOc, function ($memo, $one) use ($purchasingBySuppliers) {
            $one['oc'] += 0;
            $one['sc'] += 0;
            $one['rebate_perc'] += 0;
            $supplier = strtoupper($one['code']);

            $memo[$supplier] = $purchasingBySuppliers[$supplier] < $one['treshhold']
                ? $purchasingBySuppliers[$supplier] * (1 - $one['rebate_perc'] / 100) + $one['oc'] + $one['sc']
                : $purchasingBySuppliers[$supplier] * (1 - $one['rebate_perc'] / 100) + $one['oc'];

            return $memo;
        }, []);

        foreach ($totalsBySuppliers as $key => &$value) {
            $value = round($value, 2);
        }

        return $totalsBySuppliers;
    }

    /**
     * Calculates total purchasing for all order lines grouped by supplier
     *
     * @param array $orderDetails order lines by order_id
     * @return mixed
     */
    public function calcPurchasingBySuppliers(array $orderDetails)
    {
        $purchasingBySupplier = array_reduce($orderDetails, function ($memo, $one) {
            $supplier = strtoupper($one['Supplier']);
            if (isset($memo[$supplier])) {
                $memo[$supplier] += $one['Pprice'] * $one['Quant'];
            } else {
                $memo[$supplier] = $one['Pprice'] * $one['Quant'];
            }
            return $memo;
        });

        return $purchasingBySupplier;
    }

    /**
     * Calculates subtotal of the order
     *
     * @param array $orderDetails order lines by order_id
     * @param array $orders order by order_id
     * @return number
     */
    public function calcSubtotal(array $orderDetails, array $orders)
    {
        $shipping = $this->calcShipping($orders);
        $discount = $this->calcTotalDiscount($orderDetails);
        $orderDetailTotals = $this->calcSumOfEndUserTotals($orderDetails);

        return $orderDetailTotals + $shipping - $discount;
    }

    /**
     * Calculates sum of all end user totals
     *
     * @param array $orderDetails order lines by order_id
     * @return number
     */
    public function calcSumOfEndUserTotals(array $orderDetails)
    {
        return array_sum(array_map(function ($one) {
            return $one['EUprice'] * $one['Quant'];
        }, $orderDetails));
    }

    /**
     * Calculates total discount of the order
     *
     * @param array $orderDetails order lines by order_id
     * @return number
     */
    public function calcTotalDiscount(array $orderDetails)
    {
        return array_sum(array_map(function ($one) {
            return $one['discount'];
        }, $orderDetails));
    }

    /**
     * Calculates shipping of the order
     *
     * @param array $orders order by order_id
     * @return mixed
     */
    public function calcShipping(array $orders)
    {
        return array_reduce($orders, function ($memo, $one) {
            $shipping = $one['ShippingCosts'] + 0;
            if ($memo === 0 && $shipping > 0) {
                $memo = $shipping;
            }

            return $memo;
        }, 0);
    }
}
